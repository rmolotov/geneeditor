﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace geneeditor.Migrations
{
    public partial class ProtospacerStartPosition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Protospacer_PAMs_PAMId",
                table: "Protospacer");

            migrationBuilder.DropForeignKey(
                name: "FK_Protospacer_Virus_VirusId",
                table: "Protospacer");

            migrationBuilder.DropForeignKey(
                name: "FK_Spacers_Protospacer_ProtospacerId",
                table: "Spacers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Virus",
                table: "Virus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Protospacer",
                table: "Protospacer");

            migrationBuilder.RenameTable(
                name: "Virus",
                newName: "Viruses");

            migrationBuilder.RenameTable(
                name: "Protospacer",
                newName: "Protospacers");

            migrationBuilder.RenameIndex(
                name: "IX_Protospacer_VirusId",
                table: "Protospacers",
                newName: "IX_Protospacers_VirusId");

            migrationBuilder.RenameIndex(
                name: "IX_Protospacer_PAMId",
                table: "Protospacers",
                newName: "IX_Protospacers_PAMId");

            migrationBuilder.AddColumn<long>(
                name: "startPos",
                table: "Protospacers",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Viruses",
                table: "Viruses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Protospacers",
                table: "Protospacers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Protospacers_PAMs_PAMId",
                table: "Protospacers",
                column: "PAMId",
                principalTable: "PAMs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Protospacers_Viruses_VirusId",
                table: "Protospacers",
                column: "VirusId",
                principalTable: "Viruses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Spacers_Protospacers_ProtospacerId",
                table: "Spacers",
                column: "ProtospacerId",
                principalTable: "Protospacers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Protospacers_PAMs_PAMId",
                table: "Protospacers");

            migrationBuilder.DropForeignKey(
                name: "FK_Protospacers_Viruses_VirusId",
                table: "Protospacers");

            migrationBuilder.DropForeignKey(
                name: "FK_Spacers_Protospacers_ProtospacerId",
                table: "Spacers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Viruses",
                table: "Viruses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Protospacers",
                table: "Protospacers");

            migrationBuilder.DropColumn(
                name: "startPos",
                table: "Protospacers");

            migrationBuilder.RenameTable(
                name: "Viruses",
                newName: "Virus");

            migrationBuilder.RenameTable(
                name: "Protospacers",
                newName: "Protospacer");

            migrationBuilder.RenameIndex(
                name: "IX_Protospacers_VirusId",
                table: "Protospacer",
                newName: "IX_Protospacer_VirusId");

            migrationBuilder.RenameIndex(
                name: "IX_Protospacers_PAMId",
                table: "Protospacer",
                newName: "IX_Protospacer_PAMId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Virus",
                table: "Virus",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Protospacer",
                table: "Protospacer",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Protospacer_PAMs_PAMId",
                table: "Protospacer",
                column: "PAMId",
                principalTable: "PAMs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Protospacer_Virus_VirusId",
                table: "Protospacer",
                column: "VirusId",
                principalTable: "Virus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Spacers_Protospacer_ProtospacerId",
                table: "Spacers",
                column: "ProtospacerId",
                principalTable: "Protospacer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
