﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace geneeditor.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bacs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bacs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Proteins",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Signature = table.Column<string>(nullable: true),
                    proteinClass = table.Column<int>(nullable: false),
                    proteinType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proteins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PAMs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    SpacerId = table.Column<int>(nullable: false),
                    ProteinId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAMs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PAMs_Proteins_ProteinId",
                        column: x => x.ProteinId,
                        principalTable: "Proteins",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Spacers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    BacId = table.Column<int>(nullable: false),
                    PAMId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Spacers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Spacers_Bacs_BacId",
                        column: x => x.BacId,
                        principalTable: "Bacs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Spacers_PAMs_PAMId",
                        column: x => x.PAMId,
                        principalTable: "PAMs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PAMs_ProteinId",
                table: "PAMs",
                column: "ProteinId");

            migrationBuilder.CreateIndex(
                name: "IX_Spacers_BacId",
                table: "Spacers",
                column: "BacId");

            migrationBuilder.CreateIndex(
                name: "IX_Spacers_PAMId",
                table: "Spacers",
                column: "PAMId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Spacers");

            migrationBuilder.DropTable(
                name: "Bacs");

            migrationBuilder.DropTable(
                name: "PAMs");

            migrationBuilder.DropTable(
                name: "Proteins");
        }
    }
}
