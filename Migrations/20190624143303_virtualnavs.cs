﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace geneeditor.Migrations
{
    public partial class virtualnavs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAMs_Proteins_ProteinId",
                table: "PAMs");

            migrationBuilder.AlterColumn<int>(
                name: "ProteinId",
                table: "PAMs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PAMs_Proteins_ProteinId",
                table: "PAMs",
                column: "ProteinId",
                principalTable: "Proteins",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PAMs_Proteins_ProteinId",
                table: "PAMs");

            migrationBuilder.AlterColumn<int>(
                name: "ProteinId",
                table: "PAMs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_PAMs_Proteins_ProteinId",
                table: "PAMs",
                column: "ProteinId",
                principalTable: "Proteins",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
