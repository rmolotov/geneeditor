﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace geneeditor.Migrations
{
    public partial class Virus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Spacers_PAMs_PAMId",
                table: "Spacers");

            migrationBuilder.DropIndex(
                name: "IX_Spacers_PAMId",
                table: "Spacers");

            migrationBuilder.RenameColumn(
                name: "PAMId",
                table: "Spacers",
                newName: "ProtospacerId");

            migrationBuilder.RenameColumn(
                name: "SpacerId",
                table: "PAMs",
                newName: "ProtospacerId");

            migrationBuilder.AddColumn<string>(
                name: "ConsensusSequence",
                table: "Proteins",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "PAMs",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Virus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Virus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Protospacer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    VirusId = table.Column<int>(nullable: false),
                    PAMId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Protospacer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Protospacer_PAMs_PAMId",
                        column: x => x.PAMId,
                        principalTable: "PAMs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Protospacer_Virus_VirusId",
                        column: x => x.VirusId,
                        principalTable: "Virus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Spacers_ProtospacerId",
                table: "Spacers",
                column: "ProtospacerId");

            migrationBuilder.CreateIndex(
                name: "IX_Protospacer_PAMId",
                table: "Protospacer",
                column: "PAMId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Protospacer_VirusId",
                table: "Protospacer",
                column: "VirusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Spacers_Protospacer_ProtospacerId",
                table: "Spacers",
                column: "ProtospacerId",
                principalTable: "Protospacer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Spacers_Protospacer_ProtospacerId",
                table: "Spacers");

            migrationBuilder.DropTable(
                name: "Protospacer");

            migrationBuilder.DropTable(
                name: "Virus");

            migrationBuilder.DropIndex(
                name: "IX_Spacers_ProtospacerId",
                table: "Spacers");

            migrationBuilder.DropColumn(
                name: "ConsensusSequence",
                table: "Proteins");

            migrationBuilder.RenameColumn(
                name: "ProtospacerId",
                table: "Spacers",
                newName: "PAMId");

            migrationBuilder.RenameColumn(
                name: "ProtospacerId",
                table: "PAMs",
                newName: "SpacerId");

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "PAMs",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Spacers_PAMId",
                table: "Spacers",
                column: "PAMId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Spacers_PAMs_PAMId",
                table: "Spacers",
                column: "PAMId",
                principalTable: "PAMs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
