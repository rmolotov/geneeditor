﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace geneeditor.Migrations
{
    public partial class protosToSpacers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Spacers_Protospacers_ProtospacerId",
                table: "Spacers");

            migrationBuilder.DropIndex(
                name: "IX_Spacers_ProtospacerId",
                table: "Spacers");

            migrationBuilder.DropColumn(
                name: "ProtospacerId",
                table: "Spacers");

            migrationBuilder.AddColumn<int>(
                name: "SpacerId",
                table: "Protospacers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Protospacers_SpacerId",
                table: "Protospacers",
                column: "SpacerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Protospacers_Spacers_SpacerId",
                table: "Protospacers",
                column: "SpacerId",
                principalTable: "Spacers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Protospacers_Spacers_SpacerId",
                table: "Protospacers");

            migrationBuilder.DropIndex(
                name: "IX_Protospacers_SpacerId",
                table: "Protospacers");

            migrationBuilder.DropColumn(
                name: "SpacerId",
                table: "Protospacers");

            migrationBuilder.AddColumn<int>(
                name: "ProtospacerId",
                table: "Spacers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Spacers_ProtospacerId",
                table: "Spacers",
                column: "ProtospacerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Spacers_Protospacers_ProtospacerId",
                table: "Spacers",
                column: "ProtospacerId",
                principalTable: "Protospacers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
