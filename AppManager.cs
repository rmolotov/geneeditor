﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace geneeditor
{
    public class AppManager
    {
        #region Singleton
        private static AppManager _instance;
        public static AppManager GetInstance()
        {
            if (_instance == null) _instance = new AppManager();
            return _instance;
        }
        #endregion

#warning pls protect connString!
        public string connString = "";
        public AppContext db = new AppContext();
    }


}
