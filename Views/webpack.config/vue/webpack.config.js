const webpack = require('webpack');
const {VueLoaderPlugin} = require('vue-loader');
module.exports = {
  entry: {
    mainjs: './src/main.vue'
  },
  output: {
    path: __dirname + '../../../dist'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: ['vue-loader']
      }
    ]
  },
  plugins: [new MiniCssExtractPlugin({filename: '[name].js', chunkFilename: '[id].js'})],
  new webpack.NoEmitOnErrorsPlugin()
}
