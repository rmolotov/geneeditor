const webpack = require('webpack');
module.exports = {
  entry: {
    main: './src/main.js'
  },
  output: {
    path: __dirname + '../../../dist',
    library: '[name]'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader'
      }
    ]
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin()
  ],
  mode: 'development',
  watch: true,
  watchOptions: {
    aggregateTimeout: 100
  },
  devtool: 'source-map'
}
