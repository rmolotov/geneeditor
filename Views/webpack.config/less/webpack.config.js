const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
  entry: {
    mainjs: './src/main.js',
    maincss: './src/main.css',
    mainless: './src/main.less'
  },
  output: {
    path: __dirname + '../../../dist',
    library: '[name]'
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({filename: '[name].css', chunkFilename: '[id].css'}),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  mode: 'development',
  watch: true,
  watchOptions: {
    aggregateTimeout: 100
  },
  devtool: 'source-map'
}
