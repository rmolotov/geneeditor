const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
  entry: {
    mainjs: './src/mainjs.js',
    maincss: './src/main.css',
    mainstylus: './src/main.stylus'
  },
  output: {
    path: __dirname + '../../../dist'
  },
  module: {
    rules: [
      {
        test: /\.stylus$/,
        use: ['style-loader', 'css-loader', 'stylus-loader']
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({filename: '[name].css', chunkFilename: '[id].css'}),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  mode: 'development',
  watch: true,
  watchOptions: {
    aggregateTimeout: 100
  },
  devtool: 'source-map'
}
