﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using geneeditor.Models;
using geneeditor.Entities;
using Microsoft.EntityFrameworkCore;

namespace geneeditor.Controllers
{
    public class HomeController : Controller
    {
        
        public IActionResult Index()
        {


            // получаем объекты из бд и выводим на консоль
            return View();

        }

        
        public void TestMethod(string searchString)
        {
            ViewBag.CasInp = searchString;
        }

       

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            ViewBag.CasInp = "Cas9";

            return Redirect("~/Home/About");
        }

        public IActionResult Contact(string searchString , string searchType)
        {
            ViewData["Message"] = "Your contact page.";
            ViewBag.CasInp = searchString;
            ViewBag.Type = searchType;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
