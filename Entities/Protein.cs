﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

public enum ProteinClass
{
    I = 1, II
}
public enum ProteinType
{
    I = 1, IA, IB, IC, ID, IE, IF, IU,
    III, IIIA, IIIB, IIIC, IIID,
    IV, IVA, IVB,

    II, IIA, IIB, IIC,
    V, VI
}
namespace geneeditor.Entities
{
    public class Protein
    {
        public int Id { get; set; }
        public string Signature { get; set; }
        public ProteinClass proteinClass { get; set; }
        public ProteinType proteinType { get; set; }

        [StringLength(20)] public string ConsensusSequence { get; set; }

        public virtual ICollection<PAM> TargetPAMs { get; set; }
    }
}
