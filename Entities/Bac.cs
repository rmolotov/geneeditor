﻿using System;
using System.Collections.Generic;

namespace geneeditor.Entities
{
    public class Bac
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public ICollection<Spacer> Spacers { get; set; }
    }
}
