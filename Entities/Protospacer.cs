﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace geneeditor.Entities
{
    public class Protospacer
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public int VirusId { get; set; }
        public Virus Virus { get; set; }

        [ForeignKey("pam")]
        public int PAMId { get; set; }
        public PAM pam { get; set; }

        public long startPos { get; set; }

        public int SpacerId { get; set; }
        public Spacer Spacer { get; set; }
    }
}
