﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using geneeditor.Entities;

namespace geneeditor
{
    public class AppContext : DbContext
    {

        public DbSet<Bac> Bacs { get; set; }
        public DbSet<PAM> PAMs { get; set; }
        public DbSet<Protein> Proteins { get; set; }
        public DbSet<Spacer> Spacers { get; set; }
        public DbSet<Protospacer> Protospacers { get; set; }
        public DbSet<Virus> Viruses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=192.168.10.114;Database=test;Username=postgres;Password=123");
        }
    }
}
